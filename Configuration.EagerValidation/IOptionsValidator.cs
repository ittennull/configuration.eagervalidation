﻿namespace Configuration.EagerValidation
{
    interface IOptionsValidator
    {
        void Validate();
    }
}
